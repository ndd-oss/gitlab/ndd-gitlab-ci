require 'simplecov'
require 'simplecov-cobertura'

SimpleCov.add_filter "/.git/"
SimpleCov.add_filter "/bin/"
SimpleCov.add_filter "/dist/"
SimpleCov.add_filter "/lib/"
SimpleCov.add_filter "/tests/"

# does not work with Bashcov (?)
SimpleCov.minimum_coverage 100

SimpleCov.formatters = SimpleCov::Formatter::MultiFormatter.new(
  [
    SimpleCov::Formatter::HTMLFormatter,
    SimpleCov::Formatter::CoberturaFormatter
  ])
