
# NDD GitLab CI

A GitLab CI toolbox with:

- reusable jobs;
- reusable scripts;

## Maven

| Action                               | master | develop | feature/* | release/* | merge_request | tags | stage       |
|--------------------------------------|:------:|:-------:|:---------:|:---------:|:-------------:|:----:|-------------|
| clean                                |   X    |    X    |     X     |     X     |       X       |  X   | build       |
| compile and test (verify)            |   X    |    X    |     X     |     X     |       X       |  X   | build       |
| create documentation (site)          |   X    |    X    |     X     |     X     |       X       |  X   | build       |
| check dependencies (1)               |   X    |    X    |           |     X     |       X       |  X   | build       |
| lock dependencies (2)                |   X    |         |           |     X     |               |  X   | build       |
| run sonar analysis                   |   X    |    X    |   X (3)   |     X     |       X       |  X   | quality     |
| deploy documentation to environment  |        |    X    |     X     |     X     |       X       |      | release     |
| deploy documentation to pages        |        |         |           |           |               |  X   | release (4) |
| create a GitLab release              |        |         |           |           |               |  X   | release     |
| deploy snapshot artifacts to Central |        |    X    |           |           |               |      | release     |
| deploy release artifacts to Central  |        |         |           |           |               |  X   | release     |

Notes:

1. Dependencies check (`org.owasp:dependency-check-maven`) takes a long time to download the database; 
2. Dependencies lock (`se.vandmo:dependency-lock-maven-plugin`) is only checked for a release because it's too cumbersome otherwise;  
3. Quality gate is disabled;
4. The actual technical stage is `pages`;
