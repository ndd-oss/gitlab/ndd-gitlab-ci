#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Required variables:
# - CI_PROJECT_DIR (required by maven-configure.sh)
# - SONAR_BRANCH_NAME
# - SONAR_HOST_URL
# - SONAR_ORGANIZATION
# - SONAR_PROJECT_KEY (optional)
# ------------------------------------------------------------------------------

NDD_GITLAB_CI_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/shflags/shflags"

DEFINE_boolean "merge-request" false "Enable Sonar Pull Request Analysis"
DEFINE_boolean "quality-gate"  false "Enable Sonar Quality Gate"

# read -r -d '' FLAGS_HELP <<EOF
# Build the Maven project.
# EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  # shellcheck disable=SC1090
  source "${NDD_GITLAB_CI_DIR}/src/maven/maven-configure.sh"

  # ---------- Sonar project key

  if [[ -z ${SONAR_PROJECT_KEY+x} ]] || [[ -z "${SONAR_PROJECT_KEY}" ]]; then
    echo "No custom project key (SONAR_PROJECT_KEY) found"
    local group_id
    local artifact_id

    # shellcheck disable=SC2086
    group_id="$(mvn ${MAVEN_CLI_OPTIONS} -q -DforceStdout -Dexpression="project.groupId" help:evaluate)"
    # shellcheck disable=SC2086
    artifact_id="$(mvn ${MAVEN_CLI_OPTIONS} -q -DforceStdout -Dexpression="project.artifactId" help:evaluate)"

    local sonar_project_key="${group_id}:${artifact_id}"

  else
    echo "Custom project key (SONAR_PROJECT_KEY) found: ${SONAR_PROJECT_KEY}"
    local sonar_project_key="${SONAR_PROJECT_KEY}"
  fi

  # ---------- Sonar quality gate

  local quality_gate=false

  # shellcheck disable=SC2154
  if [[ "${FLAGS_quality_gate}" -eq "${FLAGS_TRUE}" ]]; then
    quality_gate=true
  fi

  # ---------- Sonar merge request

  # shellcheck disable=SC2154
  if [[ "${FLAGS_merge_request}" -eq "${FLAGS_TRUE}" ]]; then
    echo "Launching SonarQube analysis with:"
    echo "- sonar.host.url =${SONAR_HOST_URL}"
    echo "- sonar.organization = ${SONAR_ORGANIZATION}"
    echo "- sonar.projectKey = ${sonar_project_key}"
    echo "- sonar.pullrequest.key = ${CI_MERGE_REQUEST_IID}"
    echo "- sonar.pullrequest.branch = ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
    echo "- sonar.pullrequest.base = ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    echo "- sonar.qualitygate.wait = ${quality_gate}"

    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTIONS} \
      -Dsonar.host.url="${SONAR_HOST_URL}" \
      -Dsonar.login="${SONAR_TOKEN}" \
      -Dsonar.organization="${SONAR_ORGANIZATION}" \
      -Dsonar.projectKey="${sonar_project_key}" \
      -Dsonar.pullrequest.key="${CI_MERGE_REQUEST_IID}" \
      -Dsonar.pullrequest.branch="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}" \
      -Dsonar.pullrequest.base="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" \
      -Dsonar.qualitygate.wait="${quality_gate}" \
      sonar:sonar

  else
    echo "Launching SonarQube analysis with:"
    echo "- sonar.host.url =${SONAR_HOST_URL}"
    echo "- sonar.organization = ${SONAR_ORGANIZATION}"
    echo "- sonar.projectKey = ${sonar_project_key}"
    echo "- sonar.branch.name = ${SONAR_BRANCH_NAME}"
    echo "- sonar.qualitygate.wait = ${quality_gate}"

    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTIONS} \
      -Dsonar.host.url="${SONAR_HOST_URL}" \
      -Dsonar.login="${SONAR_TOKEN}" \
      -Dsonar.organization="${SONAR_ORGANIZATION}" \
      -Dsonar.projectKey="${sonar_project_key}" \
      -Dsonar.branch.name="${SONAR_BRANCH_NAME}" \
      -Dsonar.qualitygate.wait="${quality_gate}" \
      sonar:sonar
  fi
}

main "${@}"
