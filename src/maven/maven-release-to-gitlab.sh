#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Required variables:
# - CI_COMMIT_TAG
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

echo "Releasing Maven project to GitLab"

release-cli create \
  --name "Version ${CI_COMMIT_TAG}" \
  --tag-name "${CI_COMMIT_TAG}"
