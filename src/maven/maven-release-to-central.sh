#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Required variables:
# - CI_PROJECT_DIR (required by maven-configure.sh)
# ------------------------------------------------------------------------------

NDD_GITLAB_CI_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${NDD_GITLAB_CI_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

# enable after shflags
ndd::base::catch_more_errors_on

function main() {

  # shellcheck disable=SC1090
  source "${NDD_GITLAB_CI_DIR}/src/maven/maven-configure.sh"

  "${NDD_GITLAB_CI_DIR}/src/gitlab/gitlab-check-variables.sh" --mandatory-public-variable-names "OSS_SONATYPE_USERNAME SIGN_KEY_ID" --mandatory-private-variable-names "OSS_SONATYPE_PASSWORD SIGN_KEY SIGN_KEY_PASS"

  # Tests are disabled because they have been run in the 'maven-build.sh' during the 'build' job.
  # shellcheck disable=SC2086
  mvn ${MAVEN_CLI_OPTIONS} \
    -Dmaven.test.skip=true \
    -Ddependency-check.skip=true \
    -Dndd.deploy=true \
    clean deploy
}

main "${@}"
