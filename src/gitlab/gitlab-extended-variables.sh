#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Compute some useful variables in a GitLab CI pipeline:
# - CI_PROJECT_CHILD_NAMESPACE: the remaining part of CI_PROJECT_PATH after CI_PROJECT_ROOT_NAMESPACE (see https://gitlab.com/gitlab-org/gitlab/-/issues/350902).
# - CI_JOB_ARTIFACTS_URL: the URL of the job artifacts.
#
# This file must be sourced in a job script.
# ------------------------------------------------------------------------------

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

export CI_PROJECT_CHILD_NAMESPACE="${CI_PROJECT_PATH#"${CI_PROJECT_ROOT_NAMESPACE}"/}"

export CI_JOB_ARTIFACTS_URL="${CI_SERVER_PROTOCOL}://${CI_PROJECT_ROOT_NAMESPACE}.${CI_PAGES_DOMAIN}/-/${CI_PROJECT_CHILD_NAMESPACE}/-/jobs/${CI_JOB_ID}/artifacts"
