#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"

if ! command -v bashcov > /dev/null; then
  ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --red " ┃ Test coverage requires Bashcov (https://github.com/infertux/bashcov)         "
  ansi --bold --red " ┃ to be installed! Use 'make setup' to install required dependencies.          "
  ansi --bold --red " ┃ Aborting...                                                                  "
  ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1
fi

bashcov --root "${PROJECT_DIR}" "${PROJECT_DIR}/tests/test.sh"

coverage_float="$(grep 'coverage line-rate=' "${PROJECT_DIR}/coverage/coverage.xml" | sed -E 's/<coverage line-rate=\"([01]\.[0-9]+)\" .+/\1/')"
coverage_integer=$(echo "scale = 0; (${coverage_float} * 100) / 1" | bc -l)

if [[ ${coverage_integer} -ge 100 ]]; then
  ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --green " ┃ COVERAGE -- Perfect!                                                         "
  ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
else
  ansi --bold --red   " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  ansi --bold --red   " ┃ COVERAGE -- There is still some work to do...                                "
  ansi --bold --red   " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  exit 1
fi
