#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    ansi --bold --red " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    ansi --bold --red " ┃ TESTS -- Failed!                                                             "
    ansi --bold --red " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

for test_path in \
  "gitlab/test-gitlab-check-variables.sh" \
  "gitlab/test-gitlab-extended-variables.sh" \
  "maven/test-maven-build.sh" \
  "maven/test-maven-configure.sh" \
  "maven/test-maven-sonar.sh" \
  "maven/test-maven-release-to-central.sh" \
  "maven/test-maven-release-to-gitlab.sh"
do
  echo
  echo "━━━━━ Running '${PROJECT_DIR}/tests/${test_path}'"
  "${PROJECT_DIR}/tests/${test_path}"
done

ansi --bold --green " ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
ansi --bold --green " ┃ TESTS -- Perfect!                                                            "
ansi --bold --green " ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
