#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="gitlab-check-variables.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  export MPR1="VMPR1"
  export MPR2="VMPR2"
  export MPU1="VMPU1"
  export MPU2="VMPU2"
  export OPR1="VOPR1"
  export OPR2="VOPR2"
  export OPU1="VOPU1"
  export OPU2="VOPU2"
  export F1="VF1"
  export F2="VF2"
}

tearDown() {
  ndd::test::tearDown
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

test__succeeds_when_no_argument_is_passed() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_no_argument_is_passed"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__succeeds_when_no_argument_is_passed_in_debug_mode() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --debug > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_no_argument_is_passed"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"

  assertCapturedStdoutContains "Mandatory private environment variables ="
  assertCapturedStdoutContains "Mandatory public environment variables  ="
  assertCapturedStdoutContains "Optional private environment variables  ="
  assertCapturedStdoutContains "Optional public environment variables   ="
}


test__succeeds_when_all_mandatory_private_variable_names_are_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --mandatory-private-variable-names "MPR1 MPR2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_all_mandatory_private_variable_names_are_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR2 = \[MASKED\]"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__fails_when_a_mandatory_private_variable_name_is_not_set() {
  if "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --mandatory-private-variable-names "MPR1 INVALID_MPR" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_a_mandatory_private_variable_name_is_not_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[ERROR\] ┃ \- INVALID_MPR = MISSING!"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[FATAL\] .* Some mandatory environment variables are missing! Exiting!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}


test__succeeds_when_all_mandatory_public_variable_names_are_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --mandatory-public-variable-names "MPU1 MPU2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_all_mandatory_public_variable_names_are_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU1 = VMPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU2 = VMPU2"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__fails_when_a_mandatory_public_variable_name_is_not_set() {
  if "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --mandatory-public-variable-names "MPU1 INVALID_MPU" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_a_mandatory_public_variable_name_is_not_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU1 = VMPU1"
  assertCapturedStdoutContains "\[ERROR\] ┃ \- INVALID_MPU = MISSING!"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[FATAL\] .* Some mandatory environment variables are missing! Exiting!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}


test__succeeds_when_all_optional_private_variable_names_are_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --optional-private-variable-names "OPR1 OPR2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_all_optional_private_variable_names_are_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPR2 = \[MASKED\]"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__succeeds_when_an_optional_private_variable_name_is_not_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --optional-private-variable-names "OPR1 INVALID_OPR" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_an_optional_private_variable_name_is_not_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPR = MISSING!"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}


test__succeeds_when_all_optional_public_variable_names_are_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --optional-public-variable-names "OPU1 OPU2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_all_optional_public_variable_names_are_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPU1 = VOPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPU2 = VOPU2"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__succeeds_when_an_optional_public_variable_name_is_not_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --optional-public-variable-names "OPU1 INVALID_OPU" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_an_optional_public_variable_name_is_not_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPU1 = VOPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPU = MISSING!"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}


test__succeeds_when_no_forbidden_variable_name_is_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --forbidden-variable-names "INVALID_F1 INVALID_F2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_no_forbidden_variable_name_is_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "Forbidden environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_F1 = NOT SET"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_F2 = NOT SET"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}


test__fails_when_a_forbidden_variable_name_is_set() {
  if "${SRC_DIR}/gitlab/gitlab-check-variables.sh" --forbidden-variable-names "F1 INVALID_F" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_a_forbidden_variable_name_is_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "Forbidden environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- F1 = SET!"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_F = NOT SET"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[FATAL\] .* Some forbidden environment variables are set! Exiting!"
}


test__succeeds_when_all_mandatory_variable_names_are_set() {
  if ! "${SRC_DIR}/gitlab/gitlab-check-variables.sh" \
    --mandatory-private-variable-names "MPR1 MPR2" \
    --mandatory-public-variable-names "MPU1 MPU2" \
    --optional-private-variable-names "OPR1 INVALID_OPR" \
    --optional-public-variable-names "OPU1 INVALID_OPU" \
    --forbidden-variable-names "INVALID_F1 INVALID_F2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__succeeds_when_all_mandatory_variable_names_are_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR2 = \[MASKED\]"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU1 = VMPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU2 = VMPU2"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPR = MISSING!"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPU1 = VOPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPU = MISSING!"
  assertCapturedStdoutContains "Forbidden environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_F1 = NOT SET"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_F2 = NOT SET"
  assertCapturedStdoutContains "\[INFO \] .* All mandatory environment variables are set! Proceeding!"
  assertCapturedStdoutContains "\[INFO \] .* No forbidden environment variables is set! Proceeding!"
}

test__fails_when_mandatory_variable_names_are_not_set() {
  if "${SRC_DIR}/gitlab/gitlab-check-variables.sh" \
    --mandatory-private-variable-names "MPR1 INVALID_MPR" \
    --mandatory-public-variable-names "MPU1 INVALID_MPU" \
    --optional-private-variable-names "OPR1 INVALID_OPR" \
    --optional-public-variable-names "OPU1 INVALID_OPU" \
    --forbidden-variable-names "F1 F2" > "${captured_stdout}"; then
    print_captured_data
    fail "test__fails_when_mandatory_variable_names_are_not_set"
  fi

  assertCapturedStdoutContains "Mandatory private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[ERROR\] ┃ \- INVALID_MPR = MISSING!"
  assertCapturedStdoutContains "Mandatory public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- MPU1 = VMPU1"
  assertCapturedStdoutContains "\[ERROR\] ┃ \- INVALID_MPU = MISSING!"
  assertCapturedStdoutContains "Optional private environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPR1 = \[MASKED\]"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPR = MISSING!"
  assertCapturedStdoutContains "Optional public environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- OPU1 = VOPU1"
  assertCapturedStdoutContains "\[INFO \] ┃ \- INVALID_OPU = MISSING!"
  assertCapturedStdoutContains "Forbidden environment variables:"
  assertCapturedStdoutContains "\[INFO \] ┃ \- F1 = SET!"
  assertCapturedStdoutContains "\[INFO \] ┃ \- F2 = SET!"
  assertCapturedStdoutContains "\[FATAL\] .* Some mandatory environment variables are missing! Exiting!"
  assertCapturedStdoutContains "\[FATAL\] .* Some forbidden environment variables are set! Exiting!"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
