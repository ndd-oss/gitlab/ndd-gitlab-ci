#!/usr/bin/env bash

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../.." && pwd)"

LIB_DIR="${PROJECT_DIR}/lib"
SRC_DIR="${PROJECT_DIR}/src"
TESTS_DIR="${PROJECT_DIR}/tests"

# shellcheck disable=SC2034
NDD_TEST4B_LIB_DIR="${LIB_DIR}"
# shellcheck disable=SC2034
NDD_TEST4B_SCRIPT_NAME="maven-sonar.sh"
# shellcheck source-path=SCRIPTDIR/../../lib disable=SC1090
source "${PROJECT_DIR}/lib/ndd-test4b/ndd-test4b.sh"

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

main() {
  ndd::test::main
}

setUp() {
  ndd::test::setUp

  export CI_COMMIT_BRANCH="my/branch"
  export CI_MERGE_REQUEST_IID="MY_MR_IID"
  export CI_MERGE_REQUEST_SOURCE_BRANCH_NAME="MY_MR_SOURCE"
  export CI_MERGE_REQUEST_TARGET_BRANCH_NAME="MY_MR_TARGET"
  export CI_PROJECT_DIR="${test_directory}"
  export SONAR_BRANCH_NAME="MY_SONAR_BRANCH_NAME"
  export SONAR_HOST_URL="MY_SONAR_HOST_URL"
  export SONAR_ORGANIZATION="MY_SONAR_ORGANIZATION"
  export SONAR_TOKEN="MY_SONAR_TOKEN"
}

tearDown() {
  ndd::test::tearDown

  unset CI_COMMIT_BRANCH
  unset CI_MERGE_REQUEST_IID
  unset CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
  unset CI_MERGE_REQUEST_TARGET_BRANCH_NAME
  unset CI_PROJECT_DIR
  unset SONAR_BRANCH_NAME
  unset SONAR_HOST_URL
  unset SONAR_ORGANIZATION
  unset SONAR_PROJECT_KEY
  unset SONAR_TOKEN
}

# ------------------------------------------------------------------------------------------------ tests utilities -----

# ---------------------------------------------------------------------------------------------------------- tests -----

# ---------- on regular branch or tag

test__branch__without_custom_key_without_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  if ! "${SRC_DIR}/maven/maven-sonar.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__branch__without_custom_key_without_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "No custom project key (SONAR_PROJECT_KEY) found"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (default key, no quality gate)"
}

test__branch__with_custom_key_without_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  export SONAR_PROJECT_KEY=MY_SONAR_PROJECT_KEY

  if ! "${SRC_DIR}/maven/maven-sonar.sh" > "${captured_stdout}"; then
    print_captured_data
    fail "test__branch__with_custom_key_without_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "Custom project key (SONAR_PROJECT_KEY) found: MY_SONAR_PROJECT_KEY"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (custom key, no quality gate)"
}

test__branch__without_custom_key_with_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --quality-gate > "${captured_stdout}"; then
    print_captured_data
    fail "test__branch__without_custom_key_with_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "No custom project key (SONAR_PROJECT_KEY) found"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (default key, quality gate)"
}

test__branch__with_custom_key_with_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  export SONAR_PROJECT_KEY=MY_SONAR_PROJECT_KEY

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --quality-gate > "${captured_stdout}"; then
    print_captured_data
    fail "test__branch__with_custom_key_with_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "Custom project key (SONAR_PROJECT_KEY) found: MY_SONAR_PROJECT_KEY"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (custom key, quality gate)"
}

# ---------- on merge request

test__mr__without_custom_key_without_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --merge-request > "${captured_stdout}"; then
    print_captured_data
    fail "test__mr__without_custom_key_without_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "No custom project key (SONAR_PROJECT_KEY) found"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (merge request, default key, no quality gate)"
}

test__mr__with_custom_key_without_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  export SONAR_PROJECT_KEY=MY_SONAR_PROJECT_KEY

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --merge-request > "${captured_stdout}"; then
    print_captured_data
    fail "test__mr__with_custom_key_without_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "Custom project key (SONAR_PROJECT_KEY) found: MY_SONAR_PROJECT_KEY"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (merge request, custom key, no quality gate)"
}

test__mr__without_custom_key_with_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --merge-request --quality-gate > "${captured_stdout}"; then
    print_captured_data
    fail "test__mr__without_custom_key_with_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "No custom project key (SONAR_PROJECT_KEY) found"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (merge request, default key, quality gate)"
}

test__mr__with_custom_key_with_quality_gate() {
  add_mock_directory "${TESTS_DIR}/maven/mocks/test-maven-sonar"

  export SONAR_PROJECT_KEY=MY_SONAR_PROJECT_KEY

  if ! "${SRC_DIR}/maven/maven-sonar.sh" --merge-request --quality-gate > "${captured_stdout}"; then
    print_captured_data
    fail "test__mr__with_custom_key_with_quality_gate"
  fi

  assertCapturedStdoutContains "Printing Maven effective settings"
  assertCapturedStdoutContains "Custom project key (SONAR_PROJECT_KEY) found: MY_SONAR_PROJECT_KEY"
  assertCapturedStdoutContains "Launching SonarQube analysis with:"
  assertCapturedStdoutContains "Running Maven sonar (merge request, custom key, quality gate)"
}

# ----------------------------------------------------------------------------------------------------------------------

main "${@}"
