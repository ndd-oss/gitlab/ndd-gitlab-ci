#!/usr/bin/env bash

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ansi/ansi"
# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ndd-log4b/ndd-log4b.sh"
# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/ndd-utils4b/ndd-utils4b.sh"

# disable before shflags
ndd::base::catch_more_errors_off

# shellcheck source-path=SCRIPTDIR/.. disable=SC1090
source "${NDD_TEST4B_LIB_DIR}/shflags/shflags"

DEFINE_boolean  "debug"  false  "Enable debug mode"                        "d"
DEFINE_string   "tests"  ""     "Test functions names separated by space"  "t"

read -r -d '' FLAGS_HELP <<EOF
Test the '${NDD_TEST4B_SCRIPT_NAME}' script.
EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on

# ------------------------------------------------------------------------------------------------ tests lifecycle -----

ndd::test::main() {

  if debug_enabled; then
    ndd::logger::set_stdout_level "DEBUG"
    export NDD_TEST4B_MOCK_STDOUT_LEVEL="DEBUG"
  else
    ndd::logger::set_stdout_level "INFO"
    export NDD_TEST4B_MOCK_STDOUT_LEVEL="INFO"
  fi

  local original_path="${PATH}"

  local test_method="not set"
  local test_directory="not set"

  local captured_stdout="not set"
  local captured_stderr="not set"

  local captured_mock_commands="not set"

  # shellcheck disable=SC2154
  if [[ -z "${FLAGS_tests+x}" ]] || [[ "${FLAGS_tests}" == "" ]]; then
    # shellcheck disable=SC1090
    source "${NDD_TEST4B_LIB_DIR}/shunit2/shunit2"
  else
    # shellcheck disable=SC1090
    source "${NDD_TEST4B_LIB_DIR}/shunit2/shunit2" -- "${FLAGS_tests}"
  fi
}

ndd::test::setUp() {

  original_path="${PATH}"

  # shellcheck disable=SC2154
  test_method="${_shunit_test_}"
  log debug "Setting up test method '%s'" "${test_method}"

  test_directory="${SHUNIT_TMPDIR}/${test_method}"
  log debug "Creating test directory '%s'" "${test_directory}"
  mkdir "${test_directory}"

  captured_stdout="${test_directory}/captured_stdout.txt"
  captured_stderr="${test_directory}/captured_stderr.txt"
  rm -f "${captured_stdout}"
  rm -f "${captured_stderr}"

  captured_mock_commands="${test_directory}/captured_mock_commands.txt"
  rm -f "${captured_mock_commands}"
#  touch "${captured_mock_commands}"
  export NDD_TEST4B_CAPTURED_MOCK_FILE="${captured_mock_commands}"
}

ndd::test::tearDown() {

  PATH="${original_path}"

}

# ------------------------------------------------------------------------------------------------ tests utilities -----

debug_enabled() {
  # shellcheck disable=SC2154
  [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]
}

add_mock_directory() {
  local mock_directory="${1}"
  log debug "Adding mock directory '${mock_directory}' to PATH"
  PATH="${mock_directory}:${PATH}"
}

print_captured_data() {
  printf "\n"
  printf "Captured stdout:\n"
  printf "%s\n" "$(ndd::print::script_output_start)"
  [[ -f "${captured_stdout}" ]] && cat "${captured_stdout}" || echo "No file '${captured_stdout}'"
  printf "%s\n" "$(ndd::print::script_output_end)"

  printf "\n"
  printf "Captured stderr:\n"
  printf "%s\n" "$(ndd::print::script_output_start)"
  [[ -f "${captured_stderr}" ]] && cat "${captured_stderr}" || echo "No file '${captured_stderr}'"
  printf "%s\n" "$(ndd::print::script_output_end)"

  printf "\n"
  printf "Captured mock commands:\n"
  printf "%s\n" "$(ndd::print::script_output_start)"
  [[ -f "${captured_mock_commands}" ]] && cat "${captured_mock_commands}" || echo "No file '${captured_mock_commands}'"
  printf "%s\n" "$(ndd::print::script_output_end)"

  printf "\n"
}

# ---------- stdout

assertCapturedStdoutContains() {
  local expected_string="${1}"
  if ! grep "${expected_string}" "${captured_stdout}" >/dev/null; then
    print_captured_data
    fail "Captured stdout does not contain '${expected_string}'"
  fi
}

assertCapturedStdoutDoesNotContain() {
  local expected_string="${1}"
  if grep "${expected_string}" "${captured_stdout}" >/dev/null; then
    print_captured_data
    fail "Captured stdout contains '${expected_string}'"
  fi
}

assertCapturedStdoutMatches() {
  local expected_string="${1}"
  if ! grep -E "${expected_string}" "${captured_stdout}" >/dev/null; then
    print_captured_data
    fail "Captured stdout does not match '${expected_string}'"
  fi
}

assertCapturedStdoutDoesNotMatch() {
  local expected_string="${1}"
  if grep -E "${expected_string}" "${captured_stdout}" >/dev/null; then
    print_captured_data
    fail "Captured stdout matches '${expected_string}'"
  fi
}

# ---------- stderr

assertCapturedStderrContains() {
  local expected_string="${1}"
  if ! grep "${expected_string}" "${captured_stderr}" >/dev/null; then
    print_captured_data
    fail "Captured stderr does not contain '${expected_string}'"
  fi
}

assertCapturedStderrDoesNotContain() {
  local expected_string="${1}"
  if grep "${expected_string}" "${captured_stderr}" >/dev/null; then
    print_captured_data
    fail "Captured stderr contains '${expected_string}'"
  fi
}

assertCapturedStderrDoesNotMatch() {
  local expected_string="${1}"
  if grep -E "${expected_string}" "${captured_stderr}" >/dev/null; then
    print_captured_data
    fail "Captured stderr matches '${expected_string}'"
  fi
}

assertCapturedStderrMatches() {
  local expected_string="${1}"
  if ! grep -E "${expected_string}" "${captured_stderr}" >/dev/null; then
    print_captured_data
    fail "Captured stderr does not match '${expected_string}'"
  fi
}

# ---------- mock

assertCapturedMockCommandContains() {
  local expected_string="${1}"
  if ! grep "${expected_string}" "${captured_mock_commands}" >/dev/null; then
    print_captured_data
    fail "Mock logs does not contain '${expected_string}'"
  fi
}

assertCapturedMockCommandDoesNotContain() {
  local expected_string="${1}"
  if grep "${expected_string}" "${captured_mock_commands}" >/dev/null; then
    print_captured_data
    fail "Mock logs contains '${expected_string}'"
  fi
}

assertCapturedMockCommandMatches() {
  local expected_string="${1}"
  if ! grep -E "${expected_string}" "${captured_mock_commands}" >/dev/null; then
    print_captured_data
    fail "Mock logs does not match '${expected_string}'"
  fi
}

assertCapturedMockCommandDoesNotMatch() {
  local expected_string="${1}"
  if grep -E "${expected_string}" "${captured_mock_commands}" >/dev/null; then
    print_captured_data
    fail "Mock logs match '${expected_string}'"
  fi
}
